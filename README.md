# Multiplayer node server

## How to run it
- clone repository : git clone https://3pwood@bitbucket.org/3pwood/game-server.git
- install node_modules : npm install || yarn
- run it : npm start || yarn start

## How to play
- clone repository : git clone https://3pwood@bitbucket.org/3pwood/game-client.git
- install node_modules : npm install || yarn
- run it : npm start || yarn start
- open your browser at http://localhost:9000 (open multiple instance to add other player)

## Update 03/07/18
- Possible to switch map/scene (with buttons by now)