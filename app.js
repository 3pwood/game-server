var http = require('http');
var server = http.createServer();
var fs = require('fs');
var io = require('socket.io')(server);
var world = require('./js/server_world');

// DATABASE
let mongoist = require('mongoist');
let mongojs = require('mongojs');
let dbjs = mongojs('mongodb://stalker:h6SpG99k@ds153003.mlab.com:53003/gamedev');
let db = mongoist(dbjs);

let pack, files;

// Handle connection
io.on('connection', function (socket) {
    console.log('User connected : ' + socket.id);

    socket.emit('listModels', { kenneyKit: files, naturePack: pack });

    var room;
    var id = socket.id;
    world.addPlayer(id);
    var player = world.playerForId(id);

    socket.on('loadComplete', function () {
        db.map.find({ name: "prontera" })
            .then((map) => {
                joinWorld(map[0]);
            }).catch((err) => {
                console.log(err);
            });

        function joinWorld(location) {
            room = location.name;
            socket.join(room);
            console.log('You joined ' + room);

            player.location = room;
            // sending to the client
            socket.emit('createWorld', location);
            socket.emit('createPlayer', player);

            // sending to all clients in 'x' room except sender
            socket.to(room).emit('addOtherPlayer', player);
        }

        socket.on('requestOldPlayers', function (data) {
            for (var i = 0; i < world.players.length; i++) {
                if (world.players[i].playerId != id && world.players[i].location === data)
                    socket.emit('addOtherPlayer', world.players[i]);
            }
        });

        socket.on('updatePosition', function (data) {
            var newData = world.updatePlayerData(data);
            socket.to(room).emit('updatePosition', newData);
        });

        socket.on('playerAtk', function (data) {
            io.in(room).emit('playerAttack', data);
        });

        socket.on('changeMap', function (data) {
            socket.emit('loading');

            // Remove player for all
            io.in(player.location).emit('removeOtherPlayer', player);
            socket.leave(player.location);
            console.log('You left ' + player.location);

            db.map.find({ name: data })
                .then((map) => {
                    joinWorld(map[0]);
                }).catch((err) => {
                    console.log(err);
                });
        });

        socket.on('disconnect', function () {
            console.log('User disconnected');
            // sending to all clients in 'game' room, including sender
            io.in(room).emit('removeOtherPlayer', player);
            world.removePlayer(player);
        });
    });
});

server.on('listening', () => {
    console.log('Server is running');
    pack = fs.readdirSync('./models/NaturePack/')
        .filter(p => p.endsWith('.obj'));
    console.log('NaturePack', pack.length);
    files = fs.readdirSync('./models/kenneyKit/')
        .filter(line => line.endsWith('.obj'));
    console.log('KenneyKit', files.length);
});
server.listen(5000);
